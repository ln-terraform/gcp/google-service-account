output "service_account_id" {
  value = google_service_account.sa.name
}

output "service_account_email" {
  value = google_service_account.sa.email
}