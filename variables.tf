variable "service_account_id" {
  type = string
}

variable "display_name" {
  type    = string
  default = "Service Account"
}

variable "description" {
  type    = string
  default = ""
}

